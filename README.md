# Avaliação prática

## Tarefa

Utilizando a linguagem NodeJS, faça uma API que realize um CRUD.
É requisito a utilização de linguagem relacional SQL e também NoSQL, *preferencialmente* MySQL e MongoDB.

# Avaliação prática

## Tarefa

Faça uma API que realize uma operação de CRUD em:

1. Banco de dados relacional (SQL):
    - Soluções livres;
    - Utilizar *preferencialmente* MySQL;

2. Banco de dados não relacional (NoSQL):
    - Soluções livres;
    - Utilizar *preferencialmente* MongoDB;

## Requisitos

- Configurar o packaje.json para rodar a aplicação com comando:
```shell
$ npm start
```
- Utililização de NodeJS/ES6;
- Utilização de banco de dados relacional com SQL **e** não relacional com NoSQL;
- Requisições em formato JSON;
- Disponibilizar o codigo em um repositorio público do GitHub.

## Critérios levados em conta na avaliação:

- Conclusão da tarefa;
- Soluções inteligentes e inovadoras;
- Utilização de patterns e organização de código;
- Segurança de código;
- Utilização correta dos 'status code' no padrão RESTfull;
- Escolha das bibliotecas utilizadas;
- Maturidade de código.

# Boa sorte!
